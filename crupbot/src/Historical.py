from Constants import *
import csv

"""
This class tries to reduce de false negatives or positives and other errorrs
produced by noises and other elements we cannot escape in the vision method
from the observer with the help of the historic data 

Works with any type of object hashable and comparable 
"""
class Historical:

	"""
	Class constructor 
	"""
	def __init__(self):

		self.vote_cards = {}						#This dictionary saves how many times a card have been get, and each time is not geted it lows his value. 
		self.mute_tolerance = MUTE_TOLERANCE 		#The max value in vote cards, this will adjust the delay of ignoring or forgetting a card
		self.last_vote = {}							#This remember the last cards and theri values, this allow us to make consecutive cards grow on values faster
		
		self.beta = BETA_GROW						#How faster the consecutive cards with a certain value(suit,number) will take confidence 
													#(the higher, lesser the initial delay of new values, but higher change of false positives)
		
		self.alfa = ALFA_HISTORICAL					#The higher we will trust more the historical than the alforithm, so the delay will be higher but the noise will affect less
													#and lower we will trust more the observer, reducing delay but being more vulnerable to errors (applied for the number of cards)
		self.sn_cards = 0							#Smoothed number of cards: the average number of cards, we will round it and that will be our number of cards
		self.last_cards = []						#Wich were the last cards returned, this is used to determine if have been any changes 
		self.historic_votes = {}					#This dictonary saves all the historic for posterior analysys 
		self.counter = 0							#The counter give us information about in wich iteration we are
		self.historic_sn_cards = []					#This saves the smoothed number of cards for each iteration

	"""
	Based on the cards that gets form observer and the data from historical
	deduces wich would be the real cards 
	@input: cards detected by observer
	@output: historical/"real" cards 
	@output: boolean that indicates if the cards has changed 
	"""
	def getRealCards(self,new_cards):

		new_last_vote = {}

		for card in new_cards:

			#Check if card is in vote_cards  
			if(not self.vote_cards.has_key(card)):
				self.vote_cards[card] = 2	
				self.historic_votes[card] = {}
			else:
				
				#We recived the same card as before, so we grow up our confidence that will be a true card
				if(self.last_vote.has_key(card)):

					self.vote_cards[card] += self.last_vote[card]*self.beta+2
				
				#The card is not,consecutive we do a simple addition
				else:

					self.vote_cards[card] += 2

				#We ensure to not surprass the maximum allowed by mute tolerance
				self.vote_cards[card] = min(self.vote_cards[card],self.mute_tolerance)

			
			#We refresh the last_vote 
			new_last_vote[card] = self.vote_cards[card]


		#We save the new cards with votes
		self.last_vote = new_last_vote

		#We reduce 1 to all, then the cards that dont appaer will be downgraded by time (wich is why we add 2 indeed of 1 before)
		self.vote_cards = {k: max(v-1,0) for k,v in self.vote_cards.iteritems()}

		for card in self.vote_cards:
			self.historic_votes[card][self.counter] = self.vote_cards[card]

		#We sort the cards by their vote values 
		sorted_best_cards = sorted(self.vote_cards,key=self.vote_cards.get,reverse=True)

		#In this part we deduce the number of cards based on last smoothed value		
		n_cards = len(new_cards)
		self.sn_cards = self.sn_cards*self.alfa+n_cards*(1-self.alfa)
		self.historic_sn_cards.append(self.sn_cards)
		n_cards = int(round(self.sn_cards))
		

		#We take the deduced number of cards wich highest value of vote and returned as the real cards
		n_cards = min(len(sorted_best_cards),int(round(self.sn_cards)))
		
		#We take the definitive cards and  check if the cards has changed respect the last check
		cards  = sorted_best_cards[:n_cards]
		equal = False
		if(len(cards)==len(self.last_cards)):

			equal = True
			i = 0
			while(i<len(cards) and equal):
				equal = (cards[i] == self.last_cards[i])
				i+=1

		has_changed = not equal
		self.last_cards = cards

		self.counter +=1 

		return cards, has_changed


	def saveHistoricVote(self,name):

		#Historic of the votes
		csv_file = open(HISTORICAL_FILE_START_PATH + name + '.csv','wb')
		writter = csv.writer(csv_file, delimiter=';')
		writter.writerow(self.historic_votes.keys())

		for time in range(self.counter):
			row = []
			for historic in self.historic_votes.values():
				if(historic.has_key(time)):
					row.append(historic[time])
				else:
					row.append(0)

			writter.writerow(row)


		#Historic of the sn_cards 
		csv_file_2 = open(HISTORICAL_NCARDS_FILE_START_PATH + name + '.csv','wb')
		writter_2 = csv.writer(csv_file_2, delimiter=';')

		writter_2.writerow(['sn_cards','sn_cards_int'])

		for sn_card in self.historic_sn_cards:
			sn_card_str = str(sn_card)
			sn_card_adapted = sn_card_str.replace(".",",")
			writter_2.writerow([sn_card_adapted,int(round(sn_card))])




		
