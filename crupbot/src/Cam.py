import cv2 
import numpy as np
import requests as r

from Constants import *

"""
= CAM ===
Each cam class requires of a getFrame method wich returns
the image of the scene 
"""

"""
This class gets frames from a online stream with the given url
"""
class StreamCam:

	"""
	Class constructor
	@input: url from the stream
	"""
	def __init__(self,url):

		self.w_res = DEFAULT_W_RES			#widht resolution
		self.h_res = DEFAULT_H_RES			#heigh resolution
		self.url = url 						#url form the stream			

	"""
	Gets a frame from the stream
	@return: frame of the stream
	"""
	def getFrame(self):

		im_req = r.get(self.url, stream=True).content
		start = im_req.find(IMAGE_BYTE_STREAM_START)
		end = im_req.find(IMAGE_BYTE_STREAM_END)
		noend = True
		if start != -1 and end != -1:

			im_req = im_req[start:end] #end+2?
			im_req = np.fromstring(im_req, dtype=np.uint8)
			frame = cv2.imdecode(im_req,cv2.IMREAD_COLOR)

			return frame,noend

		else:

			noend = False
			print "Error reading from stream"
			return None,noend

"""
This class gets frames froma a video saved  
"""
class VideoFileCam:

	"""
	Class constructor
	@input: path of the video file
	"""
	def __init__(self,video_file):

		self.cap = cv2.VideoCapture(video_file)

	
	"""
	Gets a frame from the stream
	@return: frame from the video
	"""
	def getFrame(self):

		ret, frame = self.cap.read()
		return frame,ret

	"""
	Gets a frame form the stream with a specifying how many frames 
	to jump
	@input: the step to jump
	@output: frame from the video
	"""
	def nextFrame(self,step=1):

		for i in range(step):
			succes,image = self.cap.read()
		return image,succes
