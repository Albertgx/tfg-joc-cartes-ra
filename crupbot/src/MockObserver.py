import numpy as np
from Constants import *
import csv
import Card
import random as rand 
import cv2

"""
Simulates a Observer that returns the cards of the game wich
are specifyed on a CSV file 
"""
class MockObserver():

	"""
	Constructor method
	The input parameters are not used, only exist to have the same parametters
	as the real Observer
	"""
	def __init__(self,card_model,cam):

		self.last_frame = cv2.imread('../media/background_scene.png')
		self.game = []

		#Reading the simulated game
		game_file = open(GAME_FILE_PATH_MOCK,'r')
		game_reader = csv.reader(game_file,delimiter=';')

		for row in game_reader:
			if(row[0] != 'pos_x'): 		#Header
				if(row[0] == 'NEXT'):
					self.game.append('NEXT')
				else:
					if(rand.randint(0,100) > NOISE_VOID*100):
						if(rand.randint(1,100) > NOISE_ATTR*100):
							card = Card.FrenchCard(row[4],int(row[5]))
						else:
							card = Card.FrenchCard(rand.choice(MUTE_SUITS),rand.choice(MUTE_RANKS))
						box = (int(row[0]),int(row[1]),int(row[2]),int(row[3]))
						self.game.append((card,box))

		self.game_iter = iter(self.game)

	"""
	Returns the cards on the scene at this moment 
	specifyed by the csv file
	"""
	def getCards(self):

		cards = []
		boxs = []
		game_action = next(self.game_iter,None)
		if(game_action is not None):
			while(game_action != 'NEXT'):
				cards.append(game_action[0])
				boxs.append(game_action[1])
				game_action = next(self.game_iter,'NEXT')
		else:
			self.last_frame = None


		return cards ,boxs

