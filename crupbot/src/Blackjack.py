from CardModel import *
from Constants import *
from Zone import *
from Player import *
from Card import *
from Table import *

import threading
"""
This class allow us to play a BlackJack game with 
the observer outputs and it gives the information 
through the Table class 
"""
class BlackJack:

	"""
	Class constructor
	@input: observer, the observer instance that give us information about the table
	@input: n_players, number of players to play 
	@input: rounds, how many given times will you play 
	"""
	def __init__(self,observer,n_players,rounds):

		self.observer = observer
		self.card_model = FrenchCardModel()
		if(n_players < MIN_PLAYERS):
			raise Exception('Invalid number of players')
		self.n_players = n_players
		self.rounds = rounds 
		self.zones = []
		self.players = []
		self.table = None
		self.last_cards = []
		self.last_boxs = []

	"""
	With the given number of players and the information 
	about observer, we generate the game zones related to
	each player. It takes all the space to put the zones 
	"""
	def gen_zones_max(self):

		obs_frame = self.observer.last_frame
		if (obs_frame is None):
			return False 			#Zones cannot be created

		frame_w = obs_frame.shape[1]
		frame_h = obs_frame.shape[0]

		#DEALER ZONE 
		#The dealer zone will be on the front zone of the board so it will be the superior half part 

		h = frame_h//2 
		w = frame_w 
		x = 0
		y = 0

		dealer_zone = Zone(x,y,w,h,self.players[0])
		self.zones.append(dealer_zone)

		#PLAYERS ZONES 
		#The normal players zones will be distributed equally in the inferior half part 
		h = frame_h//2
		x = 0
		y = h


		n_normal_players = self.n_players
		i = 1
		while n_normal_players > i:
			w = frame_w//(n_normal_players-1)
			x = w*(i-1)
			player_zone = Zone(x,y,w,h,self.players[i])
			self.zones.append(player_zone)

			i+=1


	"""
	It plays a game and gives instructions 
	through the table class 
	"""
	def play(self):

		#CONFIGURE PLAYERS 
		dealer = Player('Dealer')
		self.players.append(dealer)
		
		for player_num in range(1,self.n_players):
			player = Player('Player_'+str(player_num+1))
			self.players.append(player)


	
		#CONFIGURE ZONES AND TABLE 
		self.gen_zones_max()
		self.table = Table('Blackjack',self.zones)

		#GAME LOGIC (MAIN LOOP)
		r = 0
		while(r<self.rounds):
			r+=1
			preparation_step_end = False
			reverse_card = FrenchCard(ukn_s,ukn_n)
			self.table.say("Starting game")
			self.table.say("Put one card face up on dealer zone, and another face down",dealer)

			while(not preparation_step_end):

				#If we notice there is a change 
				if(self.anyChange()):
					
					#We check if we have the starting distribution of cards on the board (only one card on dealer zone and an optional reverse card)
					if(len(self.zones[CROUPIER_NUM_ZON].cards)==1 or (len(self.zones[CROUPIER_NUM_ZON].cards)==2 and 
						self.zones[CROUPIER_NUM_ZON].hasCard(reverse_card,1))):
						
						preparation_step_end = True
					else:

						self.table.say("Put one card face up on dealer zone, and another face down",dealer)

			main_step_end = False
			player_zones = self.zones[CROUPIER_NUM_ZON+1:]
			active_players = self.players[CROUPIER_NUM_ZON+1:]
			self.table.say("Turn for the other players")

			while(not main_step_end):

				if(self.anyChange()):

					#We still check the cards of the dealer
					if(len(self.zones[CROUPIER_NUM_ZON].cards)==1 or (len(self.zones[CROUPIER_NUM_ZON].cards)==2 and
						self.zones[CROUPIER_NUM_ZON].hasCard(reverse_card,1))): 	

						#If all players are eliminated, then we start the last phase (that will automatically win the dealer)
						if(len(active_players)==0):
							main_step_end = True
						else:

							eliminated_players = []

							for player in active_players:
								if(player.score>21):
									eliminated_players.append(player)
									self.table.say("Score "+str(player.score)+" higher than 21, eliminated",player)

							for player in eliminated_players:
								active_players.remove(player)

					else:

						#If the dealer start taking cards and showing the second card then the main step ends
						if(len(self.zones[CROUPIER_NUM_ZON].cards)>1 and
							not self.zones[CROUPIER_NUM_ZON].hasCard(reverse_card)):

							main_step_end = True

						else:

							self.table.say("The delear must have only 2 cards one faced down and another faced up until last phase",dealer)

			self.table.say("Final step, dealer turn",dealer)
			final_step_end = False

			while(not final_step_end):

				winner = None
				temp_winner = None
				scores = map(lambda p: p.score*(int(p.score<22)),self.players)
				player_winner_index = scores[1:].index(max(scores[1:]))
				temp_winner = self.players[player_winner_index+1]

				#All players overcome the max score
				if(temp_winner.score > 21):
					winner = dealer
					self.table.say("Dealer wins with " + str(dealer.score))
					final_step_end = True

				else:
					#Draw with the dealer, dealer always win
					if(temp_winner.score <= dealer.score and dealer.score < 22 ):
						winner = dealer
						self.table.say("Dealer wins with "+str(dealer.score))
						final_step_end = True

					# Dealer has score has overcome max score
					elif (dealer.score > 21):
						winner = temp_winner
						self.table.say(winner.name + " win with " + str(winner.score))
						final_step_end = True

					# Dealer retires
					elif (len(self.zones[0].cards) == 0):
						winner = temp_winner
						self.table.say(winner.name + " win with " + str(winner.score))
						final_step_end = True


				self.anyChange()

		#Save the historic
		for zone in self.zones:
			zone.saveHistoric()

	"""
	Takes information about the observer instance and 
	shows it trough Table, it also refresh the states 
	of the game zones 
	"""
	def observe(self):

		new_cards = None
		card_boxs = None

		new_cards,card_boxs = self.observer.getCards()
		#self.table.say("Cards detected: "+str(len(new_cards)))

		if(self.observer.last_frame is not None):
			self.table.showTable(self.observer.last_frame)
			self.last_cards = new_cards
			self.last_boxs = card_boxs

		else:
			new_cards = self.last_cards
			card_boxs = self.last_boxs

		for i,card in enumerate(new_cards):
			for zone in self.zones:
				if(zone.checkIsInFull(card_boxs[i])):
					zone.new_cards.append(card)

		state = []
		for zone in self.zones:
			zone.refreshCards()
			cards = map(lambda c: str(c),zone.cards)
			state.append(cards)

		#self.table.say("Stable cards: "+str(state))
		#self.table.showStats()

	"""
	Observe the scene and refresh the game state info 
	and check if any zone has changed
	@output: if any zone has changed 
	"""
	def anyChange(self):

		self.observe()

		anyChange = False
		i = 0
		while(not anyChange and i<len(self.zones)):

			anyChange = self.zones[i].hasChange()
			i+=1
			
		return anyChange
