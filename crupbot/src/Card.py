from Constants import *

"""
= CARD ===
Each Card can be implemented as needed for the game alforithm, no method or attributes are 
required except for the complementary class CardModel for each Card
"""

"""
This class represents a card froma a French Deck
It can be identifyed with the rank/number and suit 
"""
class FrenchCard:

	"""
	Class constructor
	@input: suit and rank of the card  
	"""
	def __init__(self,suit,number):

		self.suit = suit
		self.number = number

	"""
	Str method
	Returns a string with all values translated to the format we
	commonly know this deck
	"""
	def __str__(self):

		suit_names = {DIAMONDS:'diamonds',CLUBS:'clubs',SPADES:'spades',HEARTS:'hearts',ukn_s:'?'}
		characters_conv = {ukn_n:'?',A:'A',J:'J',Q:'Q',K:'K'}
   		
		if(self.number < 2 or self.number > 10):
			
			return characters_conv[self.number]+" of "+suit_names[self.suit] 
   		
		return str(self.number)+" of "+suit_names[self.suit]

	"""
	Eq method
	"""
	def __eq__(self,card):

		return ((card.suit == self.suit) and (card.number == self.number))

	"""
	Hash method
	"""
	def __hash__(self):

		return hash(str(self))

