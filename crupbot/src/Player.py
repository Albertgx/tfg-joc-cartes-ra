"""
This class represent a player
It saves the score and name of it
"""
class Player():

	"""
	Class constructor
	@input: name of the player 
	"""
	def __init__(self,name):

		self.name = name
		self.score = 0

	"""
	Modifyes the score player 
	@input: score to add
	"""
	def modScore(self,score):

		self.score+=score

	"""
	Sets an specyfic score 
	@input: new score of the player 
	"""
	def setScore(self,score):

		self.score = score