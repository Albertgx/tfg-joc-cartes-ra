#CONSTANTS
#APPLICATION ======
N_MIN_ARGUMENTS = 2

#CAM ==============

DEFAULT_W_RES = 640
DEFAULT_H_RES = 480
DEFAULT_URL = 'http://192.168.1.48:8080/shot.jpg'
IMAGE_BYTE_STREAM_START = '\xff\xd8' 
IMAGE_BYTE_STREAM_END = '\xff\xd9'

#OBSERVER =========

FILTER_BY_ASPECT_RATIO_TOLERANCE = 0.2 
FILTER_BY_AREA_TOLERANCE = 1000
BINARY_THRESH_SEGMENTATION = 30

#CARD ============
CLUBS = 'c'
DIAMONDS = 'd'
SPADES = 's'
HEARTS = 'h'
ukn_s = '?'
A = 1
J = 11
Q = 12
K = 13
ukn_n = 0

#CARDMODELS =======
#FRENCHM

AREA_STD_FRN = 30000
ASPECT_RATIO_FRN = 0.71

#Template matching
#Regions of Interest where find the suit and number
POINT_RI_FRN = (0,0) #x,y
RELATIVE_SIZE_RI_FRN =  (0.17,0.25) #w,h

POINT_ROI_SEGM_FRN = (0.12,0.06) #x,y
RELATIVE_SIZE_ROI_SEGM_FRN = (0.78,0.91) #w,h

RELATIVE_SUIT_SIZE_FRN = (0.11,0.09) #w,h 
RELATIVE_NUMBER_SIZE_FRN = (0.13,0.18) #w,h 

THRESHOLD_SUIT = 0.68
THRESHOLD_RANK = 0.6

#Segmentation params
BINARY_THRESH = 170
MIN_BINARY_THRESH_MARGIN = 30

#Feature descriptors methods
THRESHOLD_DISTANCE_FEATURES = 0.85
THRESHOLD_COMPLEXITY_KP = 500
MIN_SEGMENT_AREA = 10
SLICE_BY = 0.75 #A quarter w/2 h/2 


TEMPLATE_CLUBS_PATH = '../media/templates/fdeck/suits/clubs.png'
TEMPLATE_HEARTS_PATH  = '../media/templates/fdeck/suits/hearts.png'
TMEPLATE_SPADES_PATH = '../media/templates/fdeck/suits/spades.png'
TEMPLATE_DIAMONDS_PATH = '../media/templates/fdeck/suits/diamonds.png'
TEMPLATE_A_PATH = '../media/templates/fdeck/numbers/A.png'
TEMPLATE_2_PATH = '../media/templates/fdeck/numbers/2.png'
TEMPLATE_3_PATH = '../media/templates/fdeck/numbers/3.png'
TEMPLATE_4_PATH = '../media/templates/fdeck/numbers/4.png'
TEMPLATE_5_PATH = '../media/templates/fdeck/numbers/5.png'
TEMPLATE_6_PATH = '../media/templates/fdeck/numbers/6.png'
TEMPLATE_7_PATH = '../media/templates/fdeck/numbers/7.png'
TEMPLATE_8_PATH = '../media/templates/fdeck/numbers/8.png'
TEMPLATE_9_PATH = '../media/templates/fdeck/numbers/9.png'
TEMPLATE_10_PATH = '../media/templates/fdeck/numbers/10.png'
TEMPLATE_J_PATH = '../media/templates/fdeck/numbers/J.png'
TEMPLATE_Q_PATH = '../media/templates/fdeck/numbers/Q.png'
TEMPLATE_K_PATH = '../media/templates/fdeck/numbers/K.png'

FULL_TEMPLATES_PATHS = ['as','2s','3s','4s','5s','6s','7s','8s','9s','10s','js','qs','ks',
'ac','2c','3c','4c','5c','6c','7c','8c','9c','10c','jc','qc','kc',
'ah','2h','3h','4h','5h','6h','7h','8h','9h','10h','jh','qh','kh',
'ad','2d','3d','4d','5d','6d','7d','8d','9d','10d','jd','qd','kd','dorso']

FULL_TEMPLATES_CARDS = [(A,SPADES),(2,SPADES),(3,SPADES),(4,SPADES),(5,SPADES),(6,SPADES),(7,SPADES),(8,SPADES),(9,SPADES),(10,SPADES),(J,SPADES),(Q,SPADES),(K,SPADES),
(A,CLUBS),(2,CLUBS),(3,CLUBS),(4,CLUBS),(5,CLUBS),(6,CLUBS),(7,CLUBS),(8,CLUBS),(9,CLUBS),(10,CLUBS),(J,CLUBS),(Q,CLUBS),(K,CLUBS),
(A,HEARTS),(2,HEARTS),(3,HEARTS),(4,HEARTS),(5,HEARTS),(6,HEARTS),(7,HEARTS),(8,HEARTS),(9,HEARTS),(10,HEARTS),(J,HEARTS),(Q,HEARTS),(K,HEARTS),
(A,DIAMONDS),(2,DIAMONDS),(3,DIAMONDS),(4,DIAMONDS),(5,DIAMONDS),(6,DIAMONDS),(7,DIAMONDS),(8,DIAMONDS),(9,DIAMONDS),(10,DIAMONDS),(J,DIAMONDS),(Q,DIAMONDS),(K,DIAMONDS),
(ukn_n,ukn_s)]

#BLACKJACK =======

MIN_PLAYERS = 2
CROUPIER_NUM_ZON = 0

#HISTORICAL ======

MUTE_TOLERANCE = 100  #>1
ALFA_HISTORICAL = 0.85 #[0-1]
BETA_GROW = 0	  	  #>1
HISTORICAL_FILE_START_PATH = '../data/historical/historic_votes_'	
HISTORICAL_NCARDS_FILE_START_PATH = '../data/historical/historic_sn_cards_'	

#MOCKOBSERVER ====

MOCK_RES_H = 1080
MOCK_RES_W = 1920
GAME_FILE_PATH_MOCK = '../data/games/game_1.csv'
NOISE_ATTR = 0.00								#Noise that change rank and suit
NOISE_VOID = 0.00								#Noise that reduce the number of cards 
MUTE_SUITS = [CLUBS,SPADES,HEARTS,DIAMONDS]
MUTE_RANKS = [A,2,3,4,5,6,7,8,9,10,J,Q,K]

#TABLE ===========

FONT_COLOR = (255,255,255)
TEXT_SHIFT_Y = 50

#GETRANDFRAMES ===

STEPS = 5
MAX_FRAMES = 500