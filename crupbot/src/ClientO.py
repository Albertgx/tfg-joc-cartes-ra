import Observer
import Cam
import cv2
import CardModel
import time

video_file_path = '../media/scene_card_2.mp4'
background_path = '../media/background_scene.png'
url = 'http://192.168.1.16:8080/shot.jpg'

background = cv2.imread(background_path,0)
#cam = Cam.VideoFileCam(video_file_path)
print "Cargando camara"
cam = Cam.StreamCam(url)
print "Cargando observador"
obs = Observer.Observer(CardModel.FrenchCardModel(), cam)
stop = False
print "Iniciando cliente"

while not stop:
    
    pre_time = time.time()
    cards,cards_box = obs.getCards()
    if obs.last_frame is None:
        break
    detec_time = time.time()-pre_time
    fps = 1/float(detec_time)
    #print "fps: "+str(int(fps))
    print "cards: "
    for card in cards:
    	print card

    for i,box in enumerate(cards_box):
        cv2.rectangle(obs.last_frame,(box[0],box[1]),(box[2]+box[0],box[1]+box[3]),(255,0,0),2)
        font = cv2.FONT_HERSHEY_SIMPLEX 
        cv2.putText(obs.last_frame,str(cards[i]),(box[0],box[1]),font,1,(255,255,255),2,cv2.LINE_AA)

    cv2.imshow('cam',obs.last_frame)
    if(obs.last_bs is not None):
        cv2.imshow('bs',obs.last_bs)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        stop = True

cv2.destroyAllWindows()
