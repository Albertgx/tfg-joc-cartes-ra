import MockObserver 
import Blackjack 


#Test with mock observer, the simulated game is selected in Constants GAME_FILE_PATH_MOCK
obs = MockObserver.MockObserver(None,None)
game = Blackjack.BlackJack(obs,3,1)
game.play()

