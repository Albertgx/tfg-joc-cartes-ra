import Observer 
import Cam
import Blackjack
import CardModel
import sys 
from Constants import *

#Aplication arguments
if( len(sys.argv) < N_MIN_ARGUMENTS+1 ):
	raise Exception('Insufficient arguments, Call the application with NUM_PLAYERS and N_ROUNDS')
players = int(sys.argv[1])
rounds = int(sys.argv[2])
rounds = min(rounds,1)

#Launching aplication
STREAM_URL = 'http://192.168.1.16:8080/shot.jpg'

card_model = CardModel.FrenchCardModel()
cam = Cam.StreamCam(STREAM_URL)
obs = Observer.Observer(card_model,cam)
game = Blackjack.BlackJack(obs,players,rounds)

game.play()
