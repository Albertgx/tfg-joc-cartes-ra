from Historical import *

"""
This class represents an area of the table wich is asociated to
one player and knows wich card are inside
"""
class Zone:

	"""
	Class constructor
	@input: posx,posy: coords for the most left higher corner of the Zone box
	@input: sizew,sizeh: size of the Zone box
	@input: player: at who is attached the zone 
	"""
	def __init__(self,posx,posy,sizew,sizeh,player=None):

		self.x = posx
		self.y = posy 
		self.w = sizew
		self.h = sizeh 

		self.cards = []
		self.new_cards = []
		self.historic = Historical()
		self.modifyed = False

		self.player = player

	def checkIsIn(self,box):

		return False

	"""
	Chacks if a box is totally inside the zones
	without collide with the zone limits 
	@input: the box to check 
	"""
	def checkIsInFull(self,box):

		bx = box[0]
		by = box[1]
		bw = box[2]
		bh = box[3]


		if(bh < self.h and bw < self.w):

			points = [(bx,by),(bx+bw,by),(bx,by+bh),(bx+bw,by+bh)]
			out = False
			i = 0
			n_points = len(points)
			
			while(n_points > i and not out):
				out = not self.isIn(points[i])
				i+=1

		return not out 

	"""
	Check if a point is inside the zone 
	@input: the point to check 
	"""
	def isIn(self,point):

		return ((point[0]>self.x and point[0]<(self.w+self.x)) and (point[1]>self.y and point[1]<(self.h+self.y)))


	"""
	Based on the historic, checks if the new cards 
	can be really new cards or noly noise. 
	Then refresh the cards allocated on zone 
	"""
	def refreshCards(self):

		self.cards,changed = self.historic.getRealCards(self.new_cards)
		self.new_cards = []

		#In order to avoid the variable to bee True only 1 iteration, we wait the game to set it false
		if(self.modifyed==False):
			self.modifyed = changed
		
		self.refreshScore()

		return self.cards 

	"""
	Checks if the zone has an specifyc card on it,
	and can check if it is a specyfic number
	@input: card, the card to search 
	@input: n_times, (optional) how many appaers the card
	@output: True if the card appears n_times 
	"""
	def hasCard(self,card,n_times=0):

		if(n_times==0):
			has_card = 1
		else:
			has_card = n_times

		n_cards = len(self.cards)
		i = 0

		while((has_card>0 or n_times!=0) and i<n_cards):
			equal = self.cards[i] == card
			if(equal):
				has_card-=1
			i+=1

		if(has_card == 0):

			return True

		return False  

	"""
	Checks if the cards have been changed or one of
	them has been taken or added 
	"""
	def hasChange(self):

		if(self.modifyed):
			self.modifyed = False
			return True
		else:
			return False

	"""
	It refresh the score from each player deppending 
	of the cards they have
	This function works for the BlackJack scoring
	"""
	def refreshScore(self):

		if(self.player is not None):

			score = 0
			aces = 0
			for card in self.cards:
				if(card.number == 1):
					aces+=1 
				else:
					score += min(card.number,10)

			if(aces > 0):

				if(score+11+(aces-1) < 22):
					score += 11+(aces-1)
				else:
					score += aces

			self.player.score = score

	"""
	Saves the histroic of cards that have been played 
	on this particular zone 
	"""
	def saveHistoric(self):

		self.historic.saveHistoricVote(self.player.name)