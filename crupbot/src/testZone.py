from Zone import *
import Card
from Constants import *


#Collision test 

def isInTest():
	scene_shape = (1920,1080)

	ZONE_SIZE_W = 300
	ZONE_SIZE_H = 100
	ZONE_POINT_X = scene_shape[0]//2-ZONE_SIZE_W//2
	ZONE_POINT_Y = scene_shape[1]//2-ZONE_SIZE_H//2


	zone_1 = Zone(ZONE_POINT_X,ZONE_POINT_Y,ZONE_SIZE_W,ZONE_SIZE_H)

	expected_returns = []

	square_1 = (0,0,20,20)
	exp_result = (False,False)
	expected_returns.append(exp_result)

	square_2 = (ZONE_POINT_X-10,ZONE_POINT_Y-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_3 = (ZONE_POINT_X+ZONE_SIZE_W/2-10,ZONE_POINT_Y-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_4 = (ZONE_POINT_X+ZONE_SIZE_W-10,ZONE_POINT_Y-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_5 = (ZONE_POINT_X-10,ZONE_POINT_Y+ZONE_SIZE_H/2-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_6 = (ZONE_POINT_X+ZONE_SIZE_W/2-10,ZONE_POINT_Y+ZONE_SIZE_H/2-10,20,20)
	exp_result = (True,True)
	expected_returns.append(exp_result)

	square_7 = (ZONE_POINT_X+ZONE_SIZE_W-10,ZONE_POINT_Y-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_8 = (ZONE_POINT_X-10,ZONE_POINT_Y+ZONE_SIZE_H-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_9 = (ZONE_POINT_X+ZONE_SIZE_W/2-10,ZONE_POINT_Y+ZONE_SIZE_H-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	square_10 =(ZONE_POINT_X+ZONE_SIZE_W-10,ZONE_POINT_Y+ZONE_SIZE_H-10,20,20)
	exp_result = (True,False)
	expected_returns.append(exp_result)

	squares = [square_1,square_2,square_3,square_4,square_5,square_6,square_7,square_8,square_9,square_10]
	
	i = 0
	for square in squares:

		ret = zone_1.checkIsIn(square)
		ret_f = zone_1.checkIsInFull(square)
		returned = (ret,ret_f)

		assert (returned[1] ==expected_returns[i][1]), "Collision from "+str(i+1)+"th square should be: "+str(expected_returns[i])+" and it's "+str(returned)
		i+=1

	

#Test if a specyfic card is into a zone 
def hasCardtest():

	card_1 = Card.FrenchCard(SPADES,5)
	card_2 = Card.FrenchCard(HEARTS,K)
	card_3 = Card.FrenchCard(SPADES,6)

	zone = Zone(0,0,20,20)
	zone.cards = [card_1,card_2,card_1]

	expected_return_1_2 = True
	expected_return_1_1 = False
	expected_return_1 = True
	expected_return_2 = True
	expected_return_2_2 = False
	expected_return_3 = False

	assert zone.hasCard(card_1,2) == expected_return_1_2
	assert zone.hasCard(card_1,1) == expected_return_1_1
	assert zone.hasCard(card_1) == expected_return_1
	assert zone.hasCard(card_2) == expected_return_2
	assert zone.hasCard(card_2,2) == expected_return_2_2
	assert zone.hasCard(card_3) == expected_return_3

"""
Testing if a zone has changed of card or taken one 
the test is afected by ALALFA_HISTORICAL and BETA_GROW
"""
def hasChange():

	card_1 = Card.FrenchCard(CLUBS,8)
	card_2 = Card.FrenchCard(SPADES,A)
	card_3 = Card.FrenchCard(HEARTS,6)

	zone = Zone(0,0,20,20)

	assert False == zone.hasChange()

	zone.new_cards.append(card_1)
	zone.refreshCards()
	zone.new_cards.append(card_1)
	zone.refreshCards()
	zone.new_cards.append(card_1)
	zone.refreshCards()
	zone.new_cards.append(card_1)
	zone.refreshCards()
	zone.new_cards.append(card_1)
	zone.refreshCards()
	zone.new_cards.append(card_1)
	zone.refreshCards()


	assert True == zone.hasChange()
	assert False == zone.hasChange()

isInTest()
print "Test isInTest() succefully"
hasCardtest()
print "Test hasCardtest() succefully"
hasChange()
print "Test hasChange() succefully"

print "Test succefully"