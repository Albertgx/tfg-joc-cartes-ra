from Constants import *
import cv2
import numpy as np
from logging import FileHandler
from vlogging import VisualRecord
import logging
import Card 

"""
= CARD MODEL ===
Each card model generates a Card object with a given image
of the template of the card. 
Each CardModel then must implement the getCard(img) method
and have the area and aspect_radio atributes for the Oberver methods
"""


"""
This class generates a card instance from the French deck
with the image of the template
"""
class FrenchCardModel:

    """
    Class constructor
    """
    def __init__(self):

        self.area  = AREA_STD_FRN                       #Area in px of the card 
        self.aspect_radio = ASPECT_RATIO_FRN            #Aspect radio of the card shape (w/h)
        self.thresh_complex = THRESHOLD_COMPLEXITY_KP   #N keypoints that must have an HDC
        self.suit_templates = []                        #Templates of the suits
        self.number_templates = []                      #Templates of the ranks/numbers
        self.index_templates = []                       #Keypoints and descriptors of card index 
        self.full_templates = []                        #Keypoints and descriptors of full card 
        self.segment_templates = []                     #Keypoints and descriptors of the center of the card
        self.slice_templates = []                       #Keypoints and descriptors of a slice of the prevoius templates
        self.feature_ext = cv2.xfeatures2d.SIFT_create(edgeThreshold=40)   #Feature extractor for the getCard method
        self.fast = cv2.FastFeatureDetector_create()
        self.suit_templates_dic = {}
        self.full_tempates_dic = {}
        self.loadTemplates()

        #Setting logger ===
        self.logger = logging.getLogger("CardModel")
        self.logger.setLevel(logging.DEBUG)
        file_handler = FileHandler("../logs/log_card.html",mode = "w")
        formatter = logging.Formatter('[%(asctime)s]:[%(levelname)s]%(name)s: %(message)s <br>')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        #Debugging
        self.last_n_kp = 0

    """
    This methods loads all the templates allocated in files
    """
    def loadTemplates(self):

        #Loading suits tempaltes
        clubs = cv2.imread(TEMPLATE_CLUBS_PATH)
        self.suit_templates.append((clubs,CLUBS))
        spades = cv2.imread(TMEPLATE_SPADES_PATH)
        self.suit_templates.append((spades, SPADES))
        diamonds = cv2.imread(TEMPLATE_DIAMONDS_PATH)
        self.suit_templates.append((diamonds, DIAMONDS))
        hearts = cv2.imread(TEMPLATE_HEARTS_PATH)
        self.suit_templates.append((hearts, HEARTS))

        for template in self.suit_templates:
            self.suit_templates_dic[template[1]] = template[0]

        #Loading rank templates 
        ace = cv2.imread(TEMPLATE_A_PATH)
        self.number_templates.append((ace,A))
        two = cv2.imread(TEMPLATE_2_PATH)
        self.number_templates.append((two,2))
        three = cv2.imread(TEMPLATE_3_PATH)
        self.number_templates.append((three,3))
        four  = cv2.imread(TEMPLATE_4_PATH)
        self.number_templates.append((four,4))
        five  = cv2.imread(TEMPLATE_5_PATH)
        self.number_templates.append((five,5))
        six = cv2.imread(TEMPLATE_6_PATH)
        self.number_templates.append((six,6))
        seven = cv2.imread(TEMPLATE_7_PATH)
        self.number_templates.append((seven,7))
        eight = cv2.imread(TEMPLATE_8_PATH)
        self.number_templates.append((eight,8))
        nine = cv2.imread(TEMPLATE_9_PATH)
        self.number_templates.append((nine,9))
        ten = cv2.imread(TEMPLATE_10_PATH)
        self.number_templates.append((ten,10))
        j = cv2.imread(TEMPLATE_J_PATH)
        self.number_templates.append((j,J))
        q = cv2.imread(TEMPLATE_Q_PATH)
        self.number_templates.append((q,Q))
        k = cv2.imread(TEMPLATE_K_PATH)
        self.number_templates.append((k,K))

        #Loading keypoints and descriptors of different areas for each card model
        for i,template_path in enumerate(FULL_TEMPLATES_PATHS):

            #KP AND DESC OF THE FULL CARD ====
            template = cv2.imread('../media/templates/fdeck/full_templates/'+template_path+'.png')
            template = cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)
            
            kp,des = self.feature_ext.detectAndCompute(template,None)
            self.full_templates.append(((kp,des),FULL_TEMPLATES_CARDS[i]))
            self.full_tempates_dic[template_path]=(kp,des)

            #KP AND DESC OF THE CARD INDEX ===
            temp_h,temp_w = template.shape
            roi_w = int(temp_w*RELATIVE_SIZE_RI_FRN[0])
            roi_h = int(temp_h*RELATIVE_SIZE_RI_FRN[1])
            template_index = template[POINT_RI_FRN[1]:POINT_RI_FRN[1]+roi_h,POINT_RI_FRN[0]:POINT_RI_FRN[0]+roi_w]

            kp, des = self.feature_ext.detectAndCompute(template_index,None)
            self.index_templates.append(((kp,des),FULL_TEMPLATES_CARDS[i]))

            #KP AND DESC OF THE CARD CENTER ===
            roi_w = int(temp_w*RELATIVE_SIZE_ROI_SEGM_FRN[0])
            roi_h = int(temp_h*RELATIVE_SIZE_ROI_SEGM_FRN[1])
            roi_x = int(temp_w*POINT_ROI_SEGM_FRN[0])
            roi_y = int(temp_h*POINT_ROI_SEGM_FRN[1])
            template_segment = template[roi_y:roi_y+roi_h,roi_x:roi_x+roi_w]

            kp, des = self.feature_ext.detectAndCompute(template_segment,None)
            self.segment_templates.append(((kp,des),FULL_TEMPLATES_CARDS[i]))


            #KP AND DESC OF A SMALLER AREA IN CARD CENTER ===
            temp_h,temp_w = template_segment.shape
            roi_w = int(temp_w*SLICE_BY)
            roi_h = int(temp_h*SLICE_BY)
            template_slice = template_segment[0:roi_h,0:roi_w]

            kp, des = self.feature_ext.detectAndCompute(template_segment,None)
            self.slice_templates.append(((kp,des),FULL_TEMPLATES_CARDS[i]))
    """
    It returns the rank of the card with the image of the card, only works with non detailed cards such as J,Q,K
    @input: image of the card center
    @output: rank of the card 
    @contours: contours found 
    """
    def getRankBySegmentation(self,card_center):

        progress = []
        min_value = np.min(card_center)

        self.logger.debug("Binary threshold: "+str(BINARY_THRESH)+" | Minimum value: "+str(min_value))
        ret,thresh = cv2.threshold(card_center,BINARY_THRESH,255,cv2.THRESH_BINARY_INV)

        progress.append(thresh)
        
        brush = np.ones((4,6),np.uint8)
        morf = cv2.erode(thresh, brush, iterations = 3)
        progress.append(morf)
        
        brush = np.ones((7,3),np.uint8)
        morf = cv2.dilate(morf,brush, iterations = 2)
        progress.append(morf)
        
        segmentation = cv2.Canny(morf,0,255)
        brush = np.ones((2,2),np.uint8)
        segmentation = cv2.dilate(segmentation,brush,iterations = 1)
        progress.append(segmentation)

        segmentation, contours, hierarchy = cv2.findContours(segmentation,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        contours = filter(lambda con: cv2.contourArea(con) > MIN_SEGMENT_AREA ,contours)
        rank = len(contours)
        
        segmentation = cv2.cvtColor(segmentation,cv2.COLOR_GRAY2BGR)
        cv2.drawContours(segmentation, contours, -1, (0,0,255), 3)

        progress.append(segmentation)

        self.logger.debug(VisualRecord("Segmentation with "+ str(rank) +" segments",progress,fmt= "png"))

        return rank,contours 

    """
    Returns the card instance of a high detail/complex card
    @input: image of the card center
    @output: card object 
    """
    def getHighCompCard(self,card_center):


        temp_h,temp_w = card_center.shape
        roi_w = int(temp_w*SLICE_BY)
        roi_h = int(temp_h*SLICE_BY)
        card_center_slice = card_center[0:roi_h,0:roi_w]

        #image sharpening
        kernel = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
        card_center_slice = cv2.filter2D(card_center_slice,-1,kernel)

        kp1, des1 = self.feature_ext.detectAndCompute(card_center_slice, None)

        index_complex_cards = [52,10,11,12,23,24,25,36,37,38,49,50,51] #? pf ? and  each JQK for each suit (and 10c)
        
        self.logger.debug("Geting a hihg complexity card")
        
        bf = cv2.BFMatcher()

        max_n_matches = 0 
        card = None
        for i_card in index_complex_cards:

            #Loading features
            kp2, des2 = self.slice_templates[i_card][0]

            #Matching and filtering 
            matches = bf.knnMatch(des1,des2,k=2)
            good = []
            th = THRESHOLD_DISTANCE_FEATURES
            for m,n in matches:
                if m.distance < th*n.distance:
                    good.append(m)

            #Taking the template with more matches
            n_matches = len(good)
            if(max_n_matches < n_matches):
                max_n_matches = n_matches
                card_data = self.slice_templates[i_card][1]

                card = Card.FrenchCard(card_data[1],card_data[0])
        if card is not None:
            
            self.logger.debug(VisualRecord("Card : "+str(card)+" with "+str(max_n_matches)+" matches",[card_center],fmt= "png"))

        return card 

    """
    Checks if a Card is a high complex/detailed card, these are the J,Q,K and the reverse of the card
    @input: the image of the center of the card
    @output: Boolean that indicates if is a detailed card or not 

    """
    def isHighCompCard(self,card_center):

        card_center_sharp = card_center.copy()
        
        #Sharpness filter (to ammount the number of feature keypoints)
        kernel = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
        card_center_sharp = cv2.filter2D(card_center_sharp,-1, kernel)

        kp = self.fast.detect(card_center_sharp,None)
        n_kp = len(kp)
        self.last_n_kp = n_kp

        card_center_sharp = cv2.drawKeypoints(card_center_sharp,kp,card_center_sharp)

        self.logger.debug("Card complexity: "+str(n_kp))
        self.logger.debug(VisualRecord("Card kp",[card_center_sharp],fmt= "png"))

        #As higher the number of kp, higher the complexity
        if(n_kp>self.thresh_complex):
            
            return True

        return False


    """
    It returns the suit of the card by the segment images 
    @input: segment image list
    @output: suit from those segments 
    """
    def getSuitBySegments(self,segments):

        suits_matches = {CLUBS:0 , SPADES:0, HEARTS:0, DIAMONDS:0}
        
        bf = cv2.BFMatcher()

        for segment in segments:

            kp1,des1 = self.feature_ext.detectAndCompute(segment,None)

            for suit in suits_matches:
                
                suit_template = self.suit_templates_dic[suit]
                suit_template = cv2.cvtColor(suit_template,cv2.COLOR_BGR2GRAY)
                kp2,des2 = self.feature_ext.detectAndCompute(suit_template,None)

                matches = bf.knnMatch(des1,des2,k=2)
                good = []
                th = THRESHOLD_DISTANCE_FEATURES
                for m,n in matches:
                    if m.distance < th*n.distance:
                        good.append(m)

                suits_matches[suit]+= len(good)

        self.logger.debug("Matches for this segment list: " + str(suits_matches))
        values = suits_matches.values()
        i_max = np.where( values == np.max(values))[0][0]

        return suits_matches.keys()[i_max]

    """
    Transforms the contours to img segments 
    @input: contours to trasnform
    @input: image that contains this contours 
    @output: segments of the image
    """
    def contours2segments(self,contours,card_center):

        #GETING THE BOXS
        segments = []

        SEG_E = 1.2 #Segment expansion: In order to take all the suit symbol we make the bounding box bigger

        for contour in contours:
            
            x,y,w,h = cv2.boundingRect(contour)
            x = int(max(0,x-((SEG_E*w)/2-w/2))) 
            y = int(max(0,y-((SEG_E*h)/2-h/2))) 
            w = int(w*SEG_E)
            h = int(h*SEG_E)

            #CROPING IMG
            segment = card_center[y:y+h,x:x+w]
            segments.append(segment)

        return segments

    """
    Returns the card instance corresponding to the img given
    @input: template of the card
    @output: Card object 
    """
    def getCard(self,img):

        #Taking image feature descriptors and keypoints

        img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        img_h,img_w = img.shape

        roi_w = int(img_w*RELATIVE_SIZE_ROI_SEGM_FRN[0])
        roi_h = int(img_h*RELATIVE_SIZE_ROI_SEGM_FRN[1])
        roi_x = int(img_w*POINT_ROI_SEGM_FRN[0])
        roi_y = int(img_h*POINT_ROI_SEGM_FRN[1])
        img_b = img[roi_y:roi_y+roi_h,roi_x:roi_x+roi_w]

        
        isComplexCard = self.isHighCompCard(img_b)
        self.logger.debug("Is a complex card: "+str(isComplexCard))

        rank = 0
        if(not isComplexCard):
            
            rank,contours = self.getRankBySegmentation(img_b)
        
        else:

            return self.getHighCompCard(img_b)


        #Matcher of the features 
        bf = cv2.BFMatcher()

        """segments = self.contours2segments(contours,img_b)
        self.logger.debug(VisualRecord("Segments: ",segments,fmt= "png"))
    
        suit = self.getSuitBySegments(segments)"""

        #Sharpening 
        kernel = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
        img = cv2.filter2D(img,-1,kernel)

        kp1_full,des1_full = self.feature_ext.detectAndCompute(img,None)

        #Full Template suit detection
        rank = min(rank,10) #Max value for LOW DETEAILED CARDS
        rank = max(rank,1) #Min value for LOW DETAILED CARDS 
        suit = None
        max_n_matches = 0
        suits = [SPADES,HEARTS,DIAMONDS,CLUBS]
        if(rank == A):
            rank_char = 'a'
        else:
            rank_char = str(rank)
        
        card_key_list = map(lambda c: rank_char+c , suits)


        for card_key in card_key_list:

            kp2,des2 = self.full_tempates_dic[card_key]

            matches = bf.knnMatch(des1_full,des2,k=2)
            good = []
            th = THRESHOLD_DISTANCE_FEATURES
            for m,n in matches: 
                if m.distance < th*n.distance:
                    good.append(m)

            n_matches = len(good)
            if(max_n_matches < n_matches):
                max_n_matches = n_matches
                
                if(rank == 10):
                    suit = card_key[2]
                else:
                    suit = card_key[1]


        card = Card.FrenchCard(suit,rank)



        return card



    def getCard_templateMatching(self,img):
        
        #Taking the ROI of the template, and the sizes of the suit and number
        temp_h,temp_w,chanels = img.shape
        
        suit_w = int(temp_w*RELATIVE_SUIT_SIZE_FRN[0])
        suit_h = int(temp_h*RELATIVE_SUIT_SIZE_FRN[1])
        
        number_w = int(temp_w*RELATIVE_SUIT_SIZE_FRN[0])
        number_h = int(temp_h*RELATIVE_SUIT_SIZE_FRN[1])
        
        roi_w = int(temp_w*RELATIVE_SIZE_RI_FRN[0])
        roi_h = int(temp_h*RELATIVE_SIZE_RI_FRN[1])

        img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        img = img[POINT_RI_FRN[0]:POINT_RI_FRN[0]+roi_h,POINT_RI_FRN[1]:POINT_RI_FRN[1]+roi_w]


        max_similar = 0
        iden_suit = ''
        iden_number = 0

        #SUIT IDENTIFY
        for templatepair in self.suit_templates:
            
            #We resize the suit template to the size of the given template suit
            template = templatepair[0]
            template = cv2.resize(template,(suit_w,suit_h))
            template = cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)

            #Template matching
            res = cv2.matchTemplate(img,template,cv2.TM_CCOEFF_NORMED)
            threshold = THRESHOLD_SUIT

            #We take the best suit aproximation
            max_suit = np.max(res)
            if max_suit>max_similar and max_suit>threshold:
                iden_suit = templatepair[1]
                max_similar = max_suit
            

        #If we have identifyed the suit
        if len(iden_suit)>0:

            max_similar_num = 0
            iden_number = 0

            #RANK IDENTIFY
            for templatepair in self.number_templates:
                
                #We resize the rank template to the size of the given template rank
                template = templatepair[0]
                template = cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)
                template = cv2.resize(template,(number_w,number_h))

                #Matching
                res = cv2.matchTemplate(img,template,cv2.TM_CCOEFF_NORMED)
                threshold = THRESHOLD_RANK

                #Best rank aproximation
                max_number = np.max(res)
                if max_number>max_similar_num and max_number>threshold:
                    iden_number = templatepair[1]
                    max_number = max_suit
          

        self.logger.debug(VisualRecord('Card: '+str(iden_number)+" of "+str(iden_suit)+' with prec: '+str(max_similar),[img], fmt = "png"))

        #If we found rank and suit
        if(iden_number>0):

            card = Card.FrenchCard(iden_suit,iden_number)
            return card 

        return None  


    def getArea(self):

        return self.area