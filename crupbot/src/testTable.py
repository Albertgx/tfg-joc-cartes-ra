import cv2 
import Player 
from Table import *
import Zone

#Visual test of the visual outputs 
img = cv2.imread('../media/background_scene.png')
max_h,max_w,chanels = img.shape

player = Player.Player('Dealer')
player_1 = Player.Player('Player_1')
player_2 = Player.Player('Player_2')

zone = Zone.Zone(0,0,max_w,max_h//2,player)
zone_1 = Zone.Zone(0,max_h//2,max_w//2,max_h//2,player_1)
zone_2 = Zone.Zone(max_w//2,max_h//2,max_w//2,max_h//2,player_2)
zones = [zone,zone_1,zone_2]

table = Table('Blackjack',zones)
table.say('Coloca una carta ',player)
table.say('Ja podeu demanar carta')
table.showTable(img)
