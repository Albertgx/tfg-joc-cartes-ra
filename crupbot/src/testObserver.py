from Observer import *
from Constants import *
from Card import FrenchCard
from CardModel import FrenchCardModel
import Cam
import numpy as np
import cv2 

def isInFrame(i_frame,freq_list):

	isIn = False
	n_freq = len(freq_list)
	i = 0
	while(not isIn and i<n_freq):
		if(freq_list[i][0] < i_frame and freq_list[i][1] > i_frame):
			isIn = True
		i+=1

	return isIn


"""
@output: value from 0 to 1,corresponding to proportion of same cards 
"""
def sameCards(cards1,cards2):

	n_cards_1 = len(cards1)
	n_cards_2 = len(cards2)
	
	if(n_cards_1 == 0 and n_cards_2 == n_cards_1):
		
		return 1 
	
	if(n_cards_1 == 0 or n_cards_2 == 0):

		return 0

	accuracy = 0
	good_cards = 0

	for card in cards1:

		car_found = False 
		i=0
		while(not car_found and i<n_cards_2):

			if(card == cards2[i]):
				car_found = True
			i += 1

		if(car_found):
			good_cards+=1 

	return float(good_cards)/float(n_cards_1)



groundtruth_path = '../media/groundtruth/'
video_file_path = '../media/scene_card_2.mp4'

deck = {'?':FrenchCard(ukn_s,ukn_n),'5c':FrenchCard(CLUBS,5),'6c':FrenchCard(CLUBS,6),'ks':FrenchCard(SPADES,K),'7s':FrenchCard(SPADES,7),
			'10c':FrenchCard(CLUBS,10),'10h':FrenchCard(HEARTS,10),'jd':FrenchCard(DIAMONDS,J),'4d':FrenchCard(DIAMONDS,4),'2d':FrenchCard(DIAMONDS,2),
			'3s':FrenchCard(SPADES,3)}

freq_deck = {'?':[(215,315),(890,910),(1860,1905)],'5c':[(375,470)],'6c':[(660,1065)],'ks':[(730,815),(960,980)],'7s':[(790,1170)],'10c':[(1420,1685)],
				'10h':[(1485,1785)],'jd':[(1560,2010),(2070,2130),(2180,2265)],'4d':[(1625,1780),(2010,2210)],'2d':[]}

n_frames = MAX_FRAMES
frame_cards = []

for i_frame in range(n_frames):
	cards = []
	for key in freq_deck:
		if(isInFrame(i_frame*STEPS,freq_deck[key])):
			cards.append(deck[key])
	frame_cards.append(cards)

cam = Cam.VideoFileCam(video_file_path)
obs = Observer(FrenchCardModel(), cam)

correct = 0
frame_count_obs = 0

for i_frame in range(n_frames):

	i_frame_step = i_frame*STEPS
	img = cv2.imread(groundtruth_path+'frame_'+str(i_frame_step)+'.jpg')
	if(img is not None):
		
		while(frame_count_obs%STEPS != 0):	
			obs_cards,boxs = obs.getCards()
			frame_count_obs+=1 

		obs_cards,boxs = obs.getCards()
		frame_count_obs+=1 

		gt_cards = frame_cards[i_frame]

		correct += sameCards(obs_cards,gt_cards)

		#cv2.imshow('frame',img)
		
		state_obs = map(lambda c: str(c),obs_cards)
		state_gt = map(lambda c: str(c),gt_cards)

		print "Groundtruth: "+str(state_gt)
		print "Obesrver: "+str(state_obs)
		#cv2.waitKey(0)
		print 'test at: '+str((float(i_frame)/float(n_frames))*100)+'%'
	else:
		raise Exception('error at loading the frame')

accuarcy = correct/n_frames
print "accuarcy :"+str(accuarcy) 

cv2.destroyAllWindows()