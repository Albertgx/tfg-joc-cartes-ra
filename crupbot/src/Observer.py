import cv2
import numpy as np 
import time
from logging import FileHandler
from vlogging import VisualRecord
import logging


from Cam import * 
from Constants import *
from Card import *
from CardModel import *

"""
This class search for cards in the scene and return it as a
card instances
"""
class Observer:

	"""
	Class constructor
	@input: the card model, the cam object, an optional background 
	"""
	def __init__(self,card_model,cam,background=None):
		
		self.cam = cam 							#Frome where we take images of the scene
		self.background = background 			#If not bakcground is given, it will take it from the start
		self.card_model = card_model			#What type of card search
		self.last_frame = cam.getFrame()[0]  	#Saves the last frame observer, is used for display how the algorithms are working
		self.last_bs = None

		#Setting logger
		self.logger = logging.getLogger("Observer")
		self.logger.setLevel(logging.DEBUG)
		file_handler = FileHandler("../logs/log.html",mode = "w")
		formatter = logging.Formatter('[%(asctime)s]:[%(levelname)s]%(name)s: %(message)s <br>')
		file_handler.setFormatter(formatter)
		self.logger.addHandler(file_handler)


	"""
	With the image of the scene returns which cards are and where
	@return: cards vector of all the cards found and it location 
	"""
	def getCards(self):
		
		self.logger.debug("========================================")
		cam_delay = time.time()
		frame,noend = self.cam.getFrame()
		cam_delay = time.time()-cam_delay
		self.logger.debug("Cam delay: "+str(cam_delay))
		self.last_frame = frame
		cards = []
		cards_box = []
		
		total_time = time.time()
		if noend:
			#FRAGMENT PHASE
			first_time = time.time()
			bboxes = self.fragment(frame)
			self.logger.debug("Fragment time: "+str(time.time()-first_time))
			
			#TRANSFORM PHASE
			first_time = time.time()
			templates = []

			for bbox in bboxes:
				template,card_box = self.transform(frame,bbox)
				if(template is not None):
					templates.append(template)
					cards_box.append(card_box)
			self.logger.debug("Transform time: "+str(time.time()-first_time))
			
			#IDENTIFY PHASE
			if(len(templates)>0):
				
				first_time = time.time()
				self.logger.debug(VisualRecord('Templates of active cards',templates, fmt = "png"))


				cards = []
				for i,template in enumerate(templates):

					card = self.identify(template)
					if(card is not None):
						cards.append(card)
						self.logger.debug("Card identifyed: "+str(card))
					else:
						cards_box[i] = None 				#We mark as None boxes that we do not identify as our type of cards

				cards_box = [box for box in cards_box if box is not None] #We delete the marked boxes 

				self.logger.debug("Identifying time"+str(time.time()-first_time))

		else: 

			self.logger.info("CAM stoped giving images")

		self.logger.debug("Recognition time: "+str(time.time()-total_time))
		self.logger.debug("========================================")

		return cards, cards_box


	"""
	Fragments the image giving us a mask with the possibles areas where to find
	the cards 
	@input: actual image of the scene
	@output: contours of the possible cards
	"""
	def fragment(self,frame):

	
		contours = None
		mask = None
		bboxes = []

		self.logger.debug("Fragmenting frame")
		grey_frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)


		
		#If background does not exist we take it from the first frame
		if self.background is None:
			self.background = grey_frame
			mask = grey_frame
		else:

			#Background substraction
			mask = abs(grey_frame-self.background)
			#self.last_bs = mask 


			#Filtering noise
			mask = cv2.GaussianBlur(mask,(1,1),2000)
			
			brush = np.ones((5,5),np.uint8)
			mask = cv2.erode(mask, brush, iterations = 3)
			brush = np.ones((10,10),np.uint8)
			mask = cv2.dilate(mask,brush,iterations = 4)
			brush = np.ones((5,5),np.uint8)
			mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, brush)
			flag, mask = cv2.threshold(mask, BINARY_THRESH_SEGMENTATION, 255, cv2.THRESH_BINARY)
			self.last_bs = mask
		

			#Edge detection
			edge = cv2.Canny(mask,0,255)


			#Finding contourns
			edge, contours , hierarchy = cv2.findContours(edge,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
			contours = filter(lambda con: cv2.contourArea(con) > self.card_model.getArea() ,contours)
			bboxes = map(lambda con: tuple(cv2.boundingRect(con)),contours)


			self.last_bs = np.copy(edge)
			self.last_bs = cv2.cvtColor(self.last_bs,cv2.COLOR_GRAY2BGR)
			cv2.drawContours(self.last_bs,contours,-1,(0,0,255),3)

		return bboxes

	"""
	Transforms the given fragment into a card template if is a card
	@input: image of the scene, and the bounding box that may contain a card
	@output: the template of the card
	"""
	def transform(self,scene,bbox):

		self.logger.debug("Transforming fragment")
		template = scene[bbox[1]:(bbox[3]+bbox[1]),bbox[0]:(bbox[2]+bbox[0]),:] #Taking fragment from image
		template = cv2.GaussianBlur(template,(5,5),0)
		brush = np.ones((2,2),np.uint8)
		edge = cv2.Canny(template,0,255)
		edge , contours , hirearchy = cv2.findContours(edge,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

		

		#TAKING THE TEMPLATE FROM FRAGMENT
		if len(contours) > 0:
			
			#Taking the contour of the card
			contour = sorted(contours, key=cv2.contourArea,reverse=True)[0]
			sub_area_contour = cv2.contourArea(contour)
			self.logger.debug("Sub area contour: "+str(sub_area_contour))
			
			#If the subcontour des not have a minimum area is eliminated
			if(sub_area_contour < self.card_model.area-FILTER_BY_AREA_TOLERANCE):
				return None,()

			#Checking aspect radio
			x,y,w,h = cv2.boundingRect(contour)
			aspect_ratio = float(w)/h
			self.logger.debug("Aspect ratio: "+str(aspect_ratio))

			

			#Adjusted bbox 
			bbox_adj = (bbox[0]+x,bbox[1]+y,w,h)
			
			#Taking the angle and rotating the working template
			rect = cv2.minAreaRect(contour)
			center = rect[0]
			angle = rect[2]
			rows, cols, chan = template.shape
			square_side = max(rows,cols)
			
			alfa = 1-np.argmax(rect[1])

			#We generate the rotation matrix
			rotation = cv2.getRotationMatrix2D(center, angle-90*alfa, 1.)
			
			abs_cos = abs(rotation[0,0])
			abs_sin = abs(rotation[0,1])

			#Then we take the size of the rotated final image
			rotated_w = int(rows*abs_sin+cols*abs_cos)
			rotated_h = int(rows*abs_cos+cols*abs_sin)

			#And finally we modify the rotation matrix changing the translation to the new center
			rotation[0,2] += rotated_w/2 - center[0] #Tx
			rotation[1,2] += rotated_h/2 - center[1] #Ty 

			#We apply the rotation 
			r_template = cv2.warpAffine(template,rotation,(rotated_w, rotated_h))

			self.logger.debug(VisualRecord('Segment: ',[template,edge,r_template], fmt = "png"))

			#Canny for edge detection and detect the template with correct rotation
			edge_r = cv2.Canny(r_template,0,255)
			edge_r , contours , hirearchy = cv2.findContours(edge_r,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
			
			if len(contours) > 0:
				contour = sorted(contours, key=cv2.contourArea,reverse=True)[0]

				rect = cv2.minAreaRect(contour)
				box = cv2.boxPoints(rect)
				box = np.int0(box)
				
				#Cuting the image to return only the template card
				sum_box = np.sum(box,axis=1)
				index_min_point = np.argmin(sum_box)
				y0 = box[index_min_point,0]
				x0 = box[index_min_point,1]
				index_max_point = np.argmax(sum_box)
				y1 = box[index_max_point,0]
				x1 = box[index_max_point,1]


				final_template = r_template[x0:x1,y0:y1,:]

				w = abs(y1-y0)
				h = abs(x1-x0)
				if(h == 0):
					return None,()

				aspect_ratio = float(w)/h
				self.logger.debug("True aspect ratio ="+str(aspect_ratio))
				area = w*h 
				self.logger.debug("True area ="+str(area))
				
				#Filter by area 
				if(area < self.card_model.area-FILTER_BY_AREA_TOLERANCE):
					return None,()
				
				#We filter by the aspect ratio
				if(aspect_ratio < self.card_model.aspect_radio - FILTER_BY_ASPECT_RATIO_TOLERANCE or
				aspect_ratio > self.card_model.aspect_radio + FILTER_BY_ASPECT_RATIO_TOLERANCE):

					return None,()


				return final_template, bbox_adj

			else:

				return r_template, bbox_adj

		return None,()

	"""
	With a given templates identify what card it is
	@input: card teamplate
	@output: card object
	"""
	def identify(self,img):

		self.logger.debug("Identifying template")
		img = cv2.GaussianBlur(img,(1,1),1500)
		card = None
		if (img is not None):
			card = self.card_model.getCard(img)
		return card

