import cv2 
import numpy as np
from Constants import *
import CardModel 


#Test the precision of the reconigtion algorithm 

file_names = ['as','2s','3s','4s','5s','6s','7s','8s','9s','10s','js','qs','ks',
'ac','2c','3c','4c','5c','6c','7c','8c','9c','10c','jc','qc','kc',
'ah','2h','3h','4h','5h','6h','7h','8h','9h','10h','jh','qh','kh',
'ad','2d','3d','4d','5d','6d','7d','8d','9d','10d','jd','qd','kd','dorso']
loaded_templates = []
groundruth_cards = [(A,SPADES),(2,SPADES),(3,SPADES),(4,SPADES),(5,SPADES),(6,SPADES),(7,SPADES),(8,SPADES),(9,SPADES),(10,SPADES),(J,SPADES),(Q,SPADES),(K,SPADES),
(A,CLUBS),(2,CLUBS),(3,CLUBS),(4,CLUBS),(5,CLUBS),(6,CLUBS),(7,CLUBS),(8,CLUBS),(9,CLUBS),(10,CLUBS),(J,CLUBS),(Q,CLUBS),(K,CLUBS),
(A,HEARTS),(2,HEARTS),(3,HEARTS),(4,HEARTS),(5,HEARTS),(6,HEARTS),(7,HEARTS),(8,HEARTS),(9,HEARTS),(10,HEARTS),(J,HEARTS),(Q,HEARTS),(K,HEARTS),
(A,DIAMONDS),(2,DIAMONDS),(3,DIAMONDS),(4,DIAMONDS),(5,DIAMONDS),(6,DIAMONDS),(7,DIAMONDS),(8,DIAMONDS),(9,DIAMONDS),(10,DIAMONDS),(J,DIAMONDS),(Q,DIAMONDS),(K,DIAMONDS),(ukn_n,ukn_s)]

for file_name in file_names:
	img = cv2.imread('../media/templates/fdeck/full_templates/'+file_name+'.png')
	loaded_templates.append(img)

print "Succefull loading templates"

french_model = CardModel.FrenchCardModel()

print "Succefull loading card model"

i = 0
correct = 0.0
total = 53.0
for template in loaded_templates:

	card = french_model.getCard(template)
	print card 
	print "========================================"
	if(card is not None):
		if(card.number==groundruth_cards[i][0] and card.suit==groundruth_cards[i][1]):
			print "Correct dientification"
			correct+=1
		else:
			print "Mistaken identification"

	
	i+=1

precision = correct/total
print "Accuarcy: "+str(precision)