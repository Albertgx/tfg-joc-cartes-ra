import logging 
import cv2
from Constants import *

"""
This class gives information to the players about the current game
through screen by text and images
"""
class Table:

	"""
	Class constructor
	@input: name, name of the game
	@input: zones, game zones located on the board
	"""
	def __init__(self,name,zones):

		self.logger = logging.getLogger(name)
		self.logger.setLevel(logging.INFO)
		formatter = logging.Formatter('[%(levelname)s]:%(name)s: %(message)s')
		handler = logging.StreamHandler()
		handler.setFormatter(formatter)
		self.logger.addHandler(handler)

		self.zones = zones

	"""
	Conmunicates to a player o multiple players an instruction
	@input: instruction to follow
	@input: to whom playre the instruction is given, if is not specifyed is for all players
	"""
	def say(self,instruction,player=None):

		if(player is None):
			
			self.logger.info('[ALL_PLAYERS]: ' + instruction)

		else:

			self.logger.info('['+ str(player.name) + ']: ' + instruction)

	"""
	Show the actual distribution of the table  with all the zones asociateds to players and 
	extra information
	@input: image of the game table
	"""
	def showTable(self,img_c):

		#img_c = img.copy()

		if(self.zones is not None):

			for zone in self.zones:

				color = (255,0,0)
				if(zone.modifyed):
					color = (0,255,0)
			
				cv2.rectangle(img_c,(zone.x,zone.y),(zone.x+zone.w,zone.y+zone.h),color,2)

				font = cv2.FONT_HERSHEY_SIMPLEX

				cv2.putText(img_c, zone.player.name+' : '+str(zone.player.score), (zone.x,zone.y+TEXT_SHIFT_Y), font, 2 , FONT_COLOR, 2, cv2.LINE_AA)
		

		cv2.imshow('Table',img_c)
		k = cv2.waitKey(1) & 0xFF
		if(k=='q'):
			cv2.destroyAllWindows()
			return True
		return False

	"""
	Shows what is the value of sn_cards in order to adjust
	alfa and know how the historical module is working 
	"""
	def showStats(self):

		if(self.zones is not None):
			
			for zone in self.zones:

				self.logger.info("Zone of ["+zone.player.name+"]: sn_cards"+str(zone.historic.sn_cards)) 




