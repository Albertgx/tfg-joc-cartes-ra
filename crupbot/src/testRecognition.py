import cv2 
import numpy as np
from Constants import *
import CardModel
from Card import * 
import csv 


def saveData(matrix,n_tests,tests,complexity,index_to_card):

	CSV_PATH = '../data/r_test/recognition_test.csv'
	CSV_PATH_2 = '../data/r_test/complexity_drop.csv'
	N_COLUMNS = 56
	
	csv_file = open(CSV_PATH,'wb')
	writer = csv.writer(csv_file, delimiter=';')
	
	#Header build 
	header = []
	cards = map(lambda t:FrenchCard(t[1],t[0]),groundruth_cards)
	header = map(lambda c: str(c),cards)
	header.insert(0,'tests')
	header.append('full_acc')
	header.append('suit_acc')
	header.append('rank_acc')

	writer.writerow(header)  

	j_test = 0

	while j_test<n_tests:

		row = matrix[j_test].tolist()
		row = map(lambda f: str(f).replace(".",","),row)
		row.insert(0,tests[j_test])

		writer.writerow(row)

		j_test+=1

	#Complexity data
	csv_file_2 = csv_file = open(CSV_PATH_2,'wb')
	writer_2 = csv.writer(csv_file, delimiter=';')

	#header
	keys = complexity.keys()
	keys = map(lambda key: str(index_to_card[key]),keys) 
	values = complexity.values()

	writer_2.writerow(keys)

	for i in range(n_tests-1):

		row = []
		for list_values in values:

			row.append(list_values[i])

		writer_2.writerow(row)



file_names = ['as','2s','3s','4s','5s','6s','7s','8s','9s','10s','js','qs','ks',
'ac','2c','3c','4c','5c','6c','7c','8c','9c','10c','jc','qc','kc',
'ah','2h','3h','4h','5h','6h','7h','8h','9h','10h','jh','qh','kh',
'ad','2d','3d','4d','5d','6d','7d','8d','9d','10d','jd','qd','kd','dorso']

loaded_templates = []

groundruth_cards = [(A,SPADES),(2,SPADES),(3,SPADES),(4,SPADES),(5,SPADES),(6,SPADES),(7,SPADES),(8,SPADES),(9,SPADES),(10,SPADES),(J,SPADES),(Q,SPADES),(K,SPADES),
(A,CLUBS),(2,CLUBS),(3,CLUBS),(4,CLUBS),(5,CLUBS),(6,CLUBS),(7,CLUBS),(8,CLUBS),(9,CLUBS),(10,CLUBS),(J,CLUBS),(Q,CLUBS),(K,CLUBS),
(A,HEARTS),(2,HEARTS),(3,HEARTS),(4,HEARTS),(5,HEARTS),(6,HEARTS),(7,HEARTS),(8,HEARTS),(9,HEARTS),(10,HEARTS),(J,HEARTS),(Q,HEARTS),(K,HEARTS),
(A,DIAMONDS),(2,DIAMONDS),(3,DIAMONDS),(4,DIAMONDS),(5,DIAMONDS),(6,DIAMONDS),(7,DIAMONDS),(8,DIAMONDS),(9,DIAMONDS),(10,DIAMONDS),(J,DIAMONDS),(Q,DIAMONDS),(K,DIAMONDS),(ukn_n,ukn_s)]

for file_name in file_names:
	img = cv2.imread('../media/templates/fdeck/full_templates/'+file_name+'.png')
	loaded_templates.append(img)

print "Succefull loading templates"

french_model = CardModel.FrenchCardModel()

print "Succefull loading card model"

#Statistics matrix 
n_max_tests = 500
statistics = np.zeros((n_max_tests,56),np.float32)
i_test = 0
tests = ["Full scale clean test"]

#Best env test ====================================================
print "Max performance test"
full_count = 0.
suit_count = 0.
rank_count = 0.
total = 53. 

for i,template in enumerate(loaded_templates): 

	card = french_model.getCard(template)
	print "========================================="
	print card
	if(card is not None):
		correct_rank = card.number == groundruth_cards[i][0]
		correct_suit = card.suit == groundruth_cards[i][1]

		full_count += int(correct_rank and correct_suit)
		suit_count += int(correct_suit)
		rank_count += int(correct_rank)

		statistics[i_test][i] = int(correct_rank and correct_suit)

		if(correct_rank and correct_suit):

			print "Correct recognition"

		else:

			print "Mistake recognition"

print "========================================="
f_accuarcy = full_count/total
print "f_accuarcy: "+str(f_accuarcy)
s_accuarcy = suit_count/total
print "s_accuarcy: "+str(s_accuarcy)
r_accuarcy = rank_count/total 
print "r_accuarcy: "+str(r_accuarcy)

statistics[i_test][53] = f_accuarcy 
statistics[i_test][54] = s_accuarcy
statistics[i_test][55] = r_accuarcy


#================================================================
#Scale test
print "Scale_test" 

i_hdc = [52,10,11,12,23,24,25,36,37,38,49,50,51] 
hdc_kp = {}
index_to_card = {}

scale_factor_dec = 0.005
scale_min = 0.25
scale_factor = 1
complx_drop = 8

while scale_factor > scale_min:
	i_test+=1 
	tests.append(str(scale_factor)+" clean test")
	french_model.thresh_complex-= complx_drop

	scale_factor -= scale_factor_dec

	full_count = 0.
	suit_count = 0.
	rank_count = 0.
	total = 53. 

	for i,template in enumerate(loaded_templates): 

		#scale change 
		template = cv2.resize(template,None,fx=scale_factor,fy=scale_factor)

		card = french_model.getCard(template)
		if(i in i_hdc):
			if(not hdc_kp.has_key(i)):
				hdc_kp[i] = []
				card_data = groundruth_cards[i]
				index_to_card[i] = FrenchCard(card_data[1],card_data[0])

			hdc_kp[i].append(french_model.last_n_kp) 

		#print "========================================="
		#print card
		if(card is not None):
			correct_rank = card.number == groundruth_cards[i][0]
			correct_suit = card.suit == groundruth_cards[i][1]

			full_count += int(correct_rank)*int(correct_suit)
			suit_count += int(correct_suit)
			rank_count += int(correct_rank)

			statistics[i_test][i] = int(correct_rank and correct_suit)


	print "========================================="
	f_accuarcy = full_count/total
	print "f_accuarcy: "+str(f_accuarcy)
	s_accuarcy = suit_count/total
	print "s_accuarcy: "+str(s_accuarcy)
	r_accuarcy = rank_count/total 
	print "r_accuarcy: "+str(r_accuarcy)

	statistics[i_test][53] = f_accuarcy 
	statistics[i_test][54] = s_accuarcy
	statistics[i_test][55] = r_accuarcy

saveData(statistics,i_test+1,tests,hdc_kp,index_to_card)
 


